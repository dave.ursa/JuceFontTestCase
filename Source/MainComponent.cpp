/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainContentComponent::MainContentComponent()
{
	typefaceOne = Typeface::createSystemTypefaceFor(BinaryData::CooperHewittSemibold_otf, BinaryData::CooperHewittSemibold_otfSize);
	typefaceTwo = Typeface::createSystemTypefaceFor(BinaryData::Leander_ttf, BinaryData::Leander_ttfSize);
    setSize (900, 100);
}

MainContentComponent::~MainContentComponent()
{
}

void MainContentComponent::paint (Graphics& g)
{
    g.fillAll (Colour (0xff001F36));

	{
		Graphics::ScopedSaveState gss(g);
		Font fontOne(typefaceOne);
		fontOne.setHeight(32);
		float hip = fontOne.getHeightInPoints();
		float h = fontOne.getHeight();

		g.setFont(fontOne);
		g.setColour(Colours::white);

		String strH(h);
		String strHip(hip);

		g.drawText("Typeface One - height: " + strH + ",  Height in Points: " + strHip, getLocalBounds(), Justification::centredTop, true);
	}


	{
		Graphics::ScopedSaveState gss(g);

		Font fontTwo(typefaceTwo);
		fontTwo.setHeight(32);
		float hip = fontTwo.getHeightInPoints();
		float h = fontTwo.getHeight();

		g.setFont(fontTwo);
		g.setColour(Colours::white);

		String strH(h);
		String strHip(hip);

		g.drawText("Typeface Two - height: " + strH + ",  Height in Points: " + strHip, getLocalBounds(), Justification::centredBottom, true);
	}
}

void MainContentComponent::resized()
{
    // This is called when the MainContentComponent is resized.
    // If you add any child components, this is where you should
    // update their positions.
}
